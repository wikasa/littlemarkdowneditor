const displayRight = document.getElementById('right-display');

document.querySelector('#notes').addEventListener('keyup', function(e){
    const changedText = toMarkdown(e.target.value);
    displayRight.innerHTML = changedText;
});

function toMarkdown(markdownText){
    const modifiers = [h1, h2, h3, h4, h5, h6, strong, em, del];
    modifiers.forEach((modifier) => {
        markdownText = markdownText.replace(modifier[0], modifier[1]);
    });
	return  markdownText;
};

const h1 = [/^# (.*$)/gim, '<h1>$1</h1>'];
const h2 = [/^## (.*$)/gim, '<h2>$1</h2>'];
const h3 = [/^### (.*$)/gim, '<h3>$1</h3>'];
const h4 = [/^#### (.*$)/gim, '<h4>$1</h4>'];
const h5 = [/^##### (.*$)/gim, '<h5>$1</h5>'];
const h6 = [/^###### (.*$)/gim, '<h6>$1</h6>'];
const em = [/[\*_](\s*\w+[\s*\w*]*?)[\*_]/gim, '<em>$1</em>'];
const strong = [/(?:__|(\*\*))(\s*\w+[\s*\w*]*?)(?:__|(\*\*))/gim, '<strong>$2</strong>'];
const del = [/~~(.*?)~~/gim, '<del>$1</del>']

let changedText = toMarkdown(displayRight.innerHTML);

displayRight.innerHTML = changedText;

//let test = console.log(toMarkdown('# test'))

const notes = ['Test 1 test test test test', 'Test 2 test test test testTest 2 test test test testTest 2 test test test test'];

const generateDOMNote = (notes) => {
    notes.forEach((note, index) => {
        const noteEl = document.createElement('div');
        noteEl.id = `note${index}`;
        noteEl.textContent += `${note.slice(0, 20)}...`;
        document.querySelector('#notes-element').appendChild(noteEl);
    });
};

generateDOMNote(notes);